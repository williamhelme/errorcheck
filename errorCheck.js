function ErrorCheck(opts) {
  this.options = {
    later: false,
    logging: true,
    interval: 0,
    priority: ['xmlhttp'], // 'socket', 'xmlhttp', 'img'
    when: ['onerror', 'called', 'interval'], // 'onerror', 'called', 'interval'
    nav: ['platform', 'userAgent', 'vendor'],
    sock: { name: 'ErrorCheck-Output', socket: null },
    xmlhttp: { method: 'POST', url: './log-js.php', parameter: 'ErrorCheck-Output' },
    img: { url: './log-js.php', parameter: 'ErrorCheck-Output' },
    func: function (){ return; }
  };

  this.errors = [];

  if(typeof opts !== 'undefined' && typeof opts !== null && typeof opts === 'object') {
    for(var k in opts) {
      this.options[k] = opts[k];
    } 
    if(this.options.when.indexOf('interval') && this.options.hasOwnProperty('interval')) {
      this.startInterval();
    }
    if(this.options.when.indexOf('onerror') !== -1) {
      this.setOnError();
    }
  }
}

ErrorCheck.prototype.init = function(func) {
  var ec = this;
  if(func instanceof Function) {
    ec.options.func = func;
    if(typeof ec.options.later === 'boolean' && ec.options.later === false) {
      ec.run(); 
    }
  } else {
    ec.log('errorCheck.js: function not passed as an argument.');
  }
};

ErrorCheck.prototype.setOnError = function() {
  var ec = this;
  window.onerror = function (message, url, line, col, err) {
    var details = ec.details();
    var info = {
      message: url + ' (' + line + '): ' + message,
      browserDetails: details
    };
    if(err && err.hasOwnProperty('stack') && message.indexOf('Script error.') === -1) {
      info.stack = err.stack; // get error stack if available
    }
    ec.errors.push(info);
    ec.report();
    if(ec.options.logging === true) {
      if(info.hasOwnProperty('stack')) {
        ec.log(info.stack);
      }else{
        ec.log(info.message);
      }
      return true;
    }
    return false;
  };
};

ErrorCheck.prototype.startInterval = function() {
  var ec = this;
  if(ec.interval > 0) { //spamming logs to yourself would just be unhelpful
    setInterval(function() {
      if(ec.errors.length > 0) { // no point reporting if there are no errors
        ec.report();
      }
    }, ec.options.interval * 1000);
  }
};

ErrorCheck.prototype.run = function() {
  var ec = this;
  if(ec.options.when instanceof Array) {
    if (ec.options.when.indexOf('called') !== -1) {
      try{
        ec.options.func();
      }catch(e) {
        var details = ec.details();
        var error = e.toString();
        ec.errors.push({
          message: error,
          browserDetails: details
        });
        if(!ec.options.hasOwnProperty('interval')) { // if there's not an interval then report straight away
          ec.report();
        }
      }
    } else {
      ec.log('errorCheck.js: the "when" array does not include the "called" option. ErrorCheck.run() has been aborted.');
    }
  } else {
    ec.log('errorCheck.js: the "when" option must be defined as an array.');
  }
};

ErrorCheck.prototype.details = function() {
  var ec = this;
  if(typeof window.navigator !== 'undefined') {
    var details = {};
    if(typeof ec.options.nav === 'object') {
      for(var n in ec.options.nav) {
        var property = ec.options.nav[n];
        if(window.navigator.hasOwnProperty(property)) {
          details[property] = window.navigator[property];
        } else {
          details[property] = 'window.navigator does not have property: ' + property + '.';
        }
      }
    }
    if(Object.keys(details).length === 0) {
      details = 'errorCheck.js: options.nav had no values specified.';
    }
    return details;
  } else {
    return 'errorCheck.js: navigator object is undefined.';
  }
};

ErrorCheck.prototype.report = function() {
  var ec = this;
  var sent = false;
  for(var x in ec.options.priority) {
    var p = ec.options.priority[x];
    if(p === 'socket') {
      if(ec.options.hasOwnProperty('sock')) {
        sent = ec.sendSocketMessage();
        if(sent === true) {
          break;  
        }
      }
    }
    if(p === 'xmlhttp') {
      if(ec.options.hasOwnProperty('xmlhttp')) {
        sent = ec.sendXMLHttpMessage();
        if(sent === true) {
          break;  
        }
      }
    }
    if(p === 'img') {
      if(ec.options.hasOwnProperty('img')) {
        sent = ec.sendImageMessage();
        if(sent === true) {
          break;  
        }
      }
    }
  }
  if(sent === false) {
    ec.log('errorCheck.js: unable to send error report to server.');
  }
};

ErrorCheck.prototype.sendSocketMessage = function() {
  var ec = this;
  if(ec.options.hasOwnProperty('sock')) {
    if(ec.options.sock.hasOwnProperty('socket')) {
      if(ec.options.sock.socket.emit instanceof Function) {
        var details = ec.details();
        ec.options.sock.socket.emit(ec.options.sock.name, { error: ec.errors, browserDetails: details });
        return true;
      }
    }
  }
  return false;
};

ErrorCheck.prototype.sendXMLHttpMessage = function() {
  var ec = this;
  if(ec.options.hasOwnProperty('xmlhttp')) {
    if(typeof ec.options.xmlhttp.url === 'string') {
      if (typeof window.XMLHttpRequest === 'function') {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open(ec.options.xmlhttp.method, ec.options.xmlhttp.url, true);
        xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xmlhttp.send(ec.options.xmlhttp.parameter + '=' + JSON.stringify(ec.errors));
        return true;
     } else {
        ec.log('errorCheck.js: can not find window.XMLHttpRequest object.');
      }
    }
  }
  return false;
};

ErrorCheck.prototype.sendImageMessage = function() {
  var ec = this;
  var img = new Image();
  var src = '';
  if(ec.options.img.hasOwnProperty('url')) {
    src = ec.options.img.url;
    src += ec.options.img.parameter + encodeURIComponent(JSON.stringify(ec.errors));
    if(img.hasOwnProperty('src')) {
      img.src = src;
      return true;
    }
  }
  return false;
};

ErrorCheck.prototype.log = function(err) {
  var ec = this;
  if(ec.options.hasOwnProperty('logging')) {
    if(ec.options.logging === true) {
      window.console.error(err);
    }
  }
};
