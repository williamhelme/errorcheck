# errorCheck.js #
### What is this repository for? ###
* This is a small script that will catch javascript errors in the browser and transmit them to the server to be stored in the system logs

### How do I get set up? ###
Just include this in your HTML file above all other scripts.
~~~~~~~html
<script type='text/javascript' src='./errorCheck.js'>
~~~~~~~
When creating an instance of ErrorCheck the second parameter takes a json object that specifies how you want the code to run.
* if you are planning on using this script to catch any errors output to `window.onerror` then it would be in the best interest for this object being created above all other scripts.
~~~~~~~js
var errControl = new ErrorCheck({'priority': ['socket', 'xmlhttp'], sock: { socket: socket } });
~~~~~~~

The parameters that can be specified within the ErrorCheck object are:

* **func (function)**:  requires a function as its parameter that can be executed straight away or executed at a later time.
* **later (boolean)**: accepts either true or false dependent on when you would like the function executed.
* **logging (boolean)**: accepts either true or false dependent on whether you would like the console errors from the module suppressed.
* **interval (seconds)**: requires a sequence of digits in the form of seconds (the module will convert the value into milliseconds for you).
* **priority (array)**:  requires an array with at least one of the following string values; 'socket', 'xmlhttp' and/or 'img'. Otherwise the errors will not be transmitted to the server.
* **when (array)**: requires an array with at least one of the following string values; `'onerror'`, `'called'` and/or `'interval'`. 
    * `'onerror'` will pick up errors transmitted to the `window.onerror` function. 
    * `'called'` will transmit errors to the server when the function passed to `.init` or passed in the initial json response is executed when calling the `.run` method. 
    * `'interval'` will transmit errors as a collection at the time the interval value was set.
* **nav (array)**: accepts an array of the keys in the web browsers navigator object you would like data from. For example: `['platform', 'userAgent', 'vendor']`.
* **sock (JSON)**: requires a JSON object with two values; a name field and the socket.io object. For example: `{ name: 'ErrorCheck-Output', socket: null }`.
* **xmlhttp (JSON)**: requires a JSON object with three values; the method of transmission (`GET`, `POST`, etc), the url at which the `XMLHTTPRequest` will be sent to and the parameter name your script will require in order to access the data. For example: `{ method: 'POST', url: './log-js.php', parameter: 'ErrorCheck-Output' }`.
* **img (JSON)**: requires a JSON object with two values; a url field and a parameter name field. For example: `{ url: './log-js.php', name: 'ErrorCheck-Output' }`.

### Server-side code ###
To store the errors in the log file here a few examples for a variety of different languages that show you how to capture and write the message sent to the server.
### PHP ###
~~~~~~~php
<?php
	$error = isset($_POST['ErrorCheck-Output']) ? $_POST['ErrorCheck-Output'] : $_GET['ErrorCheck-Output'];
	$result = exec('sh ./log-js.sh ' . $error);
	if(isset($_GET['img_request'])){
		$im = file_get_contents('./blank.png');
		header('Content-Type: image/png');
		echo $im;
	}
?>
~~~~~~~
### C#.NET ###
```
#!C#
public void JavascriptErrorLog(string sPathName)
{
    StreamWriter sw = new StreamWriter(sPathName,true);
    sw.WriteLine(sLogFormat + sErrMsg);
    sw.Flush();
    sw.Close();
}
```
### Dependencies ###
If you require sending the message via sockets then you will require the use of `socket.io` which is used with a `node.js` server.

Alternatively, if you want to send the message via `XMLHTTPRequest` or an image then you will need most modern browsers.

### Contribution guidelines ###
If you find any errors with the code and are able to find a fix, please submit the fix as a pull request.

This code is released under:

[The MIT License](http://opensource.org/licenses/MIT) (MIT)

Copyright (c) 2014 William Helme

### Who do I talk to? ###
* Repo owner: (williamhelme20@gmail.com)