var app = require('express')();
var http = require('http').Server(app);
var fs = require('fs');
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendfile('example.html');
});

app.get('/*', function (req, res) {
  // console.log(__dirname + req.path);
  if(fs.existsSync(__dirname + req.path)) { // file exists
    res.sendfile(__dirname + req.path);
  }
});

io.on('connection', function(socket){
  socket.on('ErrorCheck-Output', function(data) {
    console.log('ErrorCheckJS: ',data);
    fs.appendFile('./js-error.log', JSON.stringify(data) + '\n', function (err) {
      console.log('ErrorCheckJS: could not write to log file.');
    });
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});